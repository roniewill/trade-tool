<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission');
    }

    public function addPermission($permission)
    {
        if(is_string($permission)){
            $permission = Permission::where('name', '=', $permission)->firstOrFail();
        }

        if($this->existsPermission($permission)){
            return;
        }

        return $this->permissions()->attach($permission);
    }

    public function existsPermission($permission)
    {
        if(is_string($permission)){
            $permission = Permission::where('name', '=', $permission)->firstOrFail();
        }

        return (boolean) $this->permissions()->find($permission->id);
    }

    public function removePermission($permission)
    {
        if(is_string($permission)){
            $permission = Permission::where('name', '=', $permission)->firstOrFail();
        }

        return $this->permissions()->detach($permission);
    }
}
