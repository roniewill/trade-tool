<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Called;

class CalledPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function seeCalled($user, Called $called)
    {
        return $user->id === $called->user_id;
    }
}
