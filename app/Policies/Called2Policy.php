<?php

namespace App\Policies;

use App\User;
use App\Called;
use Illuminate\Auth\Access\HandlesAuthorization;

class Called2Policy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the called.
     *
     * @param  \App\User  $user
     * @param  \App\Called  $called
     * @return mixed
     */
    public function view(User $user, Called $called)
    {
        return $user->id === $called->user_id;
    }

    /**
     * Determine whether the user can create calleds.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the called.
     *
     * @param  \App\User  $user
     * @param  \App\Called  $called
     * @return mixed
     */
    public function update(User $user, Called $called)
    {
        //
    }

    /**
     * Determine whether the user can delete the called.
     *
     * @param  \App\User  $user
     * @param  \App\Called  $called
     * @return mixed
     */
    public function delete(User $user, Called $called)
    {
        //
    }
}
