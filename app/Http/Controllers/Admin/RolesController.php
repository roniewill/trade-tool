<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> '', 'title' => 'Roles'],
        ];
        return view('admin.roles.index', compact('roles', 'crumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> route('roles.index'), 'title' => 'Roles'],
            ['url'=> '', 'title' => 'Create'],
        ];
        return view('admin.roles.create', compact('crumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['name'] && $request['name'] != "Admin"){
            Role::create($request->all());

            return redirect()->route('roles.index');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        if($role->name == "Admin"){
            return redirect()->route('roles.index');
        }

        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> route('roles.index'), 'title' => 'Roles'],
            ['url'=> '', 'title' => 'Edit'],
        ];
        return view('admin.roles.edit', compact('crumbs', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Role::find($id)->name == "Admin"){
            return redirect()->route('roles.index');
        }

        if($request['name'] && $request['name'] != "Admin"){
            Role::find($id)->update($request->all());
        }

        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Role::find($id)->name == "Admin"){
            return redirect()->route('roles.index');
        }
        Role::find($id)->delete();
        return redirect()->route('roles.index');
    }

    /* Permissions methods */
    public function permissions($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();

        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> route('roles.index'), 'title' => 'Roles'],
            ['url'=> '', 'title' => 'Permissions']
        ];
        return view('admin.roles.permissions', compact('role', 'permissions', 'crumbs'));
    }

    public function permissionStore(Request $request, $id)
    {
        $role = Role::find($id);
        $data = $request->all();

        if($data['permission'] == 0){
            return redirect()->back();
        }

        $permission = Permission::find($data['permission']);
        $role->addPermission($permission);
        return redirect()->back();
    }

    public function permissionDestroy($id, $permission_id)
    {
        $role = Role::find($id);
        $permission = Permission::find($permission_id);
        $role->removePermission($permission);
        return redirect()->back();
    }
}
