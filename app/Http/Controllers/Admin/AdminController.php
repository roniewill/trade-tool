<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class AdminController extends Controller
{
    public function index()
    {
        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin']
        ];
        return view('admin.index', compact('crumbs'));
    }
}
