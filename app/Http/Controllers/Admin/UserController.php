<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Role;
use App\ApiAccess;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $crumbs = [
                ['url'=> '/admin', 'title' => 'Admin'],
                ['url'=> '/admin/users', 'title' => 'Users'],
            ];
        return view('admin.users.index', compact('users', 'crumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crumbs = [
                ['url'=> '/admin', 'title' => 'Admin'],
                ['url'=> '', 'title' => 'Create a new user'],
            ];
        return view('admin.users.create', compact('crumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // die(print_r($request->all()));
	$user = new User($request->all());
	$user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $api = User::find($id)->api;
        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> route('users.index'), 'title' => 'Users'],
            ['url'=> '', 'title' => 'Show'],
        ];
        return view('admin.users.show', compact('user','api','crumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> route('users.index'), 'title' => 'Users'],
            ['url'=> '', 'title' => 'Edit'],
        ];
        return view('admin.users.edit', compact('user','crumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::find($id)->update($request->all());
        return redirect()->route('users.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            User::find($id)->delete();
            return redirect()->back();
        }

        return redirect()->back();
    }

    /* Roles methods */
    public function role($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        $crumbs = [
            ['url'=> '/admin', 'title' => 'Admin'],
            ['url'=> '/admin/users', 'title' => 'Users'],
            ['url'=> '', 'title' => 'Roles']
        ];
        return view('admin.users.roles', compact('user', 'roles', 'crumbs'));
    }

    public function roleStore(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->all();

        if($data['role'] == 0){
            return redirect()->back();
        }

        $role = Role::find($data['role']);
        $user->addRole($role);
        return redirect()->back();
    }

    public function roleDestroy($id, $role_id)
    {
        $user = User::find($id);
        $role = Role::find($role_id);
        $user->removeRole($role);
        return redirect()->back();
    }
}
