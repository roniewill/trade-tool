<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ApiAccess;
use App\User;

class ApiAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crumbs = [
          ['url'=> '/admin', 'title' => 'Admin'],
          ['url'=> route('trade.index'), 'title' => 'Trade'],
          ['url'=> '', 'title' => 'Add'],
        ];
        return view('admin.trade.create', compact('crumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request['user_id'];
        ApiAccess::create($request->all());
        return redirect(route('user.show', $user_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $api = User::find($id)->api;
        $crumbs = [
          ['url'=> '/admin', 'title' => 'Admin'],
          ['url'=> route('trade.index'), 'title' => 'Trade'],
          ['url'=> '', 'title' => 'Edit'],
        ];
        return view('admin.trade.edit', compact('crumbs', 'api'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $api = User::find()->api;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $api = User::find($id)->api;
        if($api){
            ApiAccess::where('user_id', $api->user_id)->delete();
            return redirect()->back();
        }
    }
}
