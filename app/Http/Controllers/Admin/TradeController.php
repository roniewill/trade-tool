<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Admin\ApiAccess;

use App\User;

use Htunlogic\Poloniex\Poloniex;

class TradeController extends Controller
{

    public function index()
    {
        $crumbs = [
          ['url'=> '/admin', 'title' => 'Admin'],
          ['url'=> route('trade.index'), 'title' => 'Trade']
        ];
        $pairs = Poloniex::getTradingPairs();
        return view('admin.trade.trade', compact('crumbs', 'pairs'));
    }

    public function buyCoin(Request $request)
    {
        if($request->ajax()){
            $data = $request->all();
            $res = Poloniex::buy($data['buy-pair'], $data['buy-rate'], $data['buy-amount']);
            return $res;
        }
    }

    public function sellCoin(Request $request)
    {
        if($request->ajax()){
            $data = $request->all();
            $res = Poloniex::sell($data['sell-pair'], $data['sell-rate'], $data['sell-amount']);
            return $res;
        }
    }

    public function balances()
    {
        $data = Poloniex::getBalances();
        return $data;
    }

    public function getTickerByPair($pair)
    {
        $ticker = Poloniex::getTicker($pair);
        return Response()->json($ticker);
    }

    public function getOrders($pair)
    {
        $orders = Poloniex::getOrderBook($pair, 30);
        echo "<pre>";
        print_r($orders);
    }

    public function apiTest()
    {
        $api = User::find(Auth::user()->id)->api;
        dd($api);
    }

}
