<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Called;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        //$calleds = Called::where('user_id', '=', $user->id)->get();
        $calleds = Called::all();
        return view('home', compact('calleds'));
    }

    public function show($id)
    {
        $called = Called::find($id);
        //$this->authorize('see-called', $called);
        return view('show', compact('called'));
    }
}
