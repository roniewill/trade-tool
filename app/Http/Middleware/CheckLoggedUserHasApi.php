<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use App\User;

class CheckLoggedUserHasApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api = User::find(Auth::user()->id)->api;
        
        if(is_null($api)){
            return redirect()->route('api.create');
        }

        config(['poloniex.auth.key' => $api->key, 'poloniex.auth.secret' => $api->secret]);
        return $next($request);
    }
}
