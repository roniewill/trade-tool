<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiAccess extends Model
{
    protected $table = 'api_access';

    protected $fillable = ['key', 'secret', 'user_id'];
}
