<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function api()
    {
        return $this->hasOne('App\ApiAccess');
    }

    public function isAdmin()
    {
        //return $this->id == 1;
        return $this->existsRole('Admin');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function addRole($role)
    {
        if(is_string($role)){
            $role = Role::where('name', '=', $role)->firstOrFail();
        }

        if($this->existsRole($role)){
            return;
        }

        return $this->roles()->attach($role);
    }

    public function existsRole($role)
    {
        if(is_string($role)){
            $role = Role::where('name', '=', $role)->firstOrFail();
        }

        return (boolean) $this->roles()->find($role->id);
    }

    public function removeRole($role)
    {
        if(is_string($role)){
            $role = Role::where('name', '=', $role)->firstOrFail();
        }

        return $this->roles()->detach($role);
    }
    public function haveThisSameRoles($roles)
    {
        $userRoles = $this->roles;
        return $roles->intersect($userRoles)->count();
    }
}
