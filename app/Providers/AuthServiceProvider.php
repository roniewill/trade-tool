<?php

namespace App\Providers;
use App\Role;
use App\Permission;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Called' => 'App\Policies\Called2Policy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
	/*
        $this->registerPolicies();

        foreach ($this->permissionList() as $permission) {
            Gate::define($permission->name, function($user) use($permission){
                return $user->haveThisSameRoles($permission->roles) || $user->isAdmin();
            });
        }
	*/
    }

    public function permissionList()
    {
        return Permission::with('roles')->get();
    }
}
