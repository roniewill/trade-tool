<?php

use Illuminate\Database\Seeder;

use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions for users
        $create_user = Permission::firstOrCreate([
            'name' => 'create-user',
            'description' => 'Create a user on system'
        ]);

        $view_user = Permission::firstOrCreate([
            'name' => 'view-user',
            'description' => 'View a user on system'
        ]);

        $edit_user = Permission::firstOrCreate([
            'name' => 'edit-user',
            'description' => 'Edit a user on system'
        ]);

        $delete_user = Permission::firstOrCreate([
            'name' => 'delete-user',
            'description' => 'Delete a user on system'
        ]);

        // Permissions for called
        $create_called = Permission::firstOrCreate([
            'name' => 'create-called',
            'description' => 'Create a called on system'
        ]);

        $view_called = Permission::firstOrCreate([
            'name' => 'view-called',
            'description' => 'View a called on system'
        ]);

        $edit_called = Permission::firstOrCreate([
            'name' => 'edit-called',
            'description' => 'Edit a called on system'
        ]);

        $delete_called = Permission::firstOrCreate([
            'name' => 'delete-called',
            'description' => 'Delete a called on system'
        ]);


    }
}
