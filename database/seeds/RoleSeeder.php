<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::firstOrCreate([
            'name' => 'Admin',
            'description' => 'System total access'
        ]);

        $manager = Role::firstOrCreate([
            'name' => 'Manager',
            'description' => 'Management to system'
        ]);

        $user = Role::firstOrCreate([
            'name' => 'User',
            'description' => 'Common user of system'
        ]);
    }
}
