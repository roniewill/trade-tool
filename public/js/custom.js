(function($){
    $("#tickers").click(function(){
        $.ajax({
            url: "trade/tickers",
            type: "GET",
            beforeSend: function(){
                $("#img-load").css({"display":"block"});
            },
            success: function(data){
                $("#result").html(data);
                $("#img-load").css({"display":"none"});
            }
        });

        return false;
    });

    $("#volume").click(function(){
        $.ajax({
            url: "trade/volume",
            type: "GET",
            beforeSend: function(){
                $("#img-load").css({"display":"block"});
            },
            success: function(data){
                $("#result").html(data);
                $("#img-load").css({"display":"none"});
            }
        });

        return false;
    });

    $("#balances").click(function(){
        $.ajax({
            url: "trade/balances",
            type: "GET",
            beforeSend: function(){
                $("#img-load").css({"display":"block"});
            },
            success: function(data){
                console.log("Data: " + data + "\nStatus: " + status);
                $("#img-load").css({"display":"none"});
            }
        });

        return false;
    });

    $("#tradingPairs").click(function(){
        $.ajax({
            url: "trade/trading-pairs",
            type: "GET",
            beforeSend: function(){
                $("#img-load").css({"display":"block"});
            },
            success: function(data){
                $("#result").html(data);
                $("#img-load").css({"display":"none"});
            }
        });
        return false;
    });

    // On change select option value
    $("[name=pairs]").change(function(){
        var pair = $(this).val();
        var coin = pair.slice(4);

        $(".label-default").html(coin);
        $("[name=buy-pair]").val(pair);

        var url = "trade/get-ticker/"+pair;
        $.get( url, function( data ) {
            $("#sell-rate").val(data.last);
            $("#buy-rate").val(data.lowestAsk);
        });
    });

    // To calc buy value and sell value
    $("#buy-amount").keyup(function(event){
        if($(this).val() != ""){
            var rate = $("#buy-rate").val();
            var amount = $(this).val();
            var total = rate * amount;
            $("#buy-total").val(total);
        }else{
            $("#buy-total").val();
        }
    });

    $("#sell-amount").keyup(function(event){
        if($(this).val() != ""){
            var rate = $("#sell-rate").val();
            var amount = $(this).val();
            var total = rate * amount;
            $("#sell-total").val(total);
        }else{
            $("#sell-total").val();
        }
    });

    // On click to buy
    $("#btn-buy").click(function(event){
        event.preventDefault();
        var buy = $("[name=buy-form]").serialize();
        $.ajax({
            url: 'trade/buy',
            type: 'POST',
            data: buy,
            success: function(data){
                if(data.error){
                    alert(data.error);
                }
                console.log(data);
            }
        });
    });

    // On click to sell
    $("#btn-sell").click(function(event){
        event.preventDefault();
        var buy = $("[name=sell-form]").serialize();
        $.ajax({
            url: 'trade/sell',
            type: 'POST',
            data: buy,
            success: function(data){
                if(data.error){
                    alert(data.error);
                }
                console.log(data);
            }
        });
    });

})(jQuery);
