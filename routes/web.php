<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>'auth', 'prefix'=>'admin'], function(){
    Route::get('/', 'Admin\AdminController@index');
    Route::get('/trade', ['as'=>'trade.index', 'uses'=>'Admin\TradeController@index']);
    Route::post('/trade/buy', ['as'=>'trade.buy', 'uses'=>'Admin\TradeController@buyCoin']);
    Route::post('/trade/sell', ['as'=>'trade.sell', 'uses'=>'Admin\TradeController@sellCoin']);
    Route::get('/trade/tickers', 'Admin\TradeController@tickers');
    Route::get('/trade/volume', 'Admin\TradeController@volume');
    Route::get('/trade/balances', 'Admin\TradeController@balances');
    Route::get('/trade/trading-pairs', 'Admin\TradeController@tradingPairs');

    Route::get('/trade/add-api', ['as'=>'api.create', 'uses'=>'Admin\ApiAccessController@create']);
    Route::post('/trade/add-api', ['as'=>'api.store', 'uses'=>'Admin\ApiAccessController@store']);
    Route::get('/trade/edit-api/{id}', ['as'=>'api.edit', 'uses'=>'Admin\ApiAccessController@edit']);
    Route::post('/trade/edit-api', ['as'=>'api.update', 'uses'=>'Admin\ApiAccessController@update']);
    Route::get('/trade/delete-api/{id}', ['as'=>'api.destroy', 'uses'=>'Admin\ApiAccessController@destroy']);

    // Only to TEST
    Route::get('/trade/get-ticker/{pair}', 'Admin\TradeController@getTickerByPair');
    Route::get('/trade/get-orders/{pair}', 'Admin\TradeController@getOrders');
    Route::get('/trade/test', 'Admin\TradeController@apiTest');
    // End Test

    Route::resource('/users', 'Admin\UserController');
    Route::get('/user/show/{id}', ['as'=>'user.show', 'uses'=>'Admin\UserController@show']);
    Route::get('/users/role/{id}', ['as'=>'user.role', 'uses'=>'Admin\UserController@role']);
    Route::post('/users/role/{role}', ['as'=>'user.role.store', 'uses'=>'Admin\UserController@roleStore']);
    Route::delete('/users/role/{user}/{role}', ['as'=>'user.role.destroy', 'uses'=>'Admin\UserController@roleDestroy']);

    Route::get('/roles/permission/{id}', ['as'=>'role.permission', 'uses'=>'Admin\RolesController@permissions']);
    Route::post('/roles/permission/{permission}', ['as'=>'role.permission.store', 'uses'=>'Admin\RolesController@permissionStore']);
    Route::delete('/roles/permission/{role}/{permission}', ['as'=>'role.permission.destroy', 'uses'=>'Admin\RolesController@permissionDestroy']);

    Route::resource('/roles', 'Admin\RolesController');
    Route::resource('/permissions', 'Admin\PermissionsController');
});
