<div class="col-md-12">
    <ol class="breadcrumb">
        @if(isset($crumbs))
            @foreach($crumbs as $crumb)
                @if($crumb['url'])
                <li><a href="{{ $crumb['url'] }}">{{ $crumb['title'] }}</a></li>
                @else
                <li><a href="#">{{ $crumb['title'] }}</a></li>
                @endif
            @endforeach
        @else
            <li><a href="#">Admin</a></li>
        @endif
    </ol>
</div>
