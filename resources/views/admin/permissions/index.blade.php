@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Permissions List</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($actions as $action)
                        <tr>
                            <th>{{ $action->id }}</th>
                            <td>{{ $action->name }}</td>
                            <td>{{ $action->description }}</td>
                            <td>{{ $action->created_at }}</td>
                        </tr>
                    @empty
                        <p>No data registered</p>
                    @endforelse
                    </tbody>
                </table>
            </div>

        </div>
        </div>
    </div>
</div>
@endsection
