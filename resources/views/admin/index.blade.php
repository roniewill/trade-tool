@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Users</h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ route('users.index') }}"> Access all users</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Roles</h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ route('roles.index') }}"> Access all roles</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Permissions</h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ route('permissions.index') }}"> Access all permissions</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>TRADE</strong> <i class="glyphicon glyphicon-bitcoin"></i></h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/trade') }}" class="btn btn-success"> Ready Go!!!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
