@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Create Role</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

        	<div class="col-md-12">
        		<form action="{{ route('roles.store') }}" method="post">

        			{{csrf_field()}}
        			@include('admin.roles._form')

        	        <button class="btn btn-success">Save</button>

        		</form>

        	</div>
        </div>
    </div>

</div>

@endsection
