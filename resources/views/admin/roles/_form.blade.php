<div class="form-group">
    <label for="name">Role name</label>
    <input type="text" class="form-control" name="name" value="{{ isset($role->name) ? $role->name : '' }}">
</div>

<div class="form-group">
    <label for="description">Description</label>
    <input type="text" class="form-control" name="description" value="{{ isset($role->description) ? $role->description : '' }}">
</div>
