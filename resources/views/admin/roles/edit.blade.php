@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Edit Role</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

        	<div class="col-md-12">
        		<form action="{{ route('roles.update', $role->id) }}" method="post">

        			{{csrf_field()}}
                    {{ method_field('PUT') }}
        			@include('admin.roles._form')

        	        <button class="btn btn-success">Update</button>

        		</form>

        	</div>
        </div>
    </div>

</div>

@endsection
