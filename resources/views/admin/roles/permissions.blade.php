@extends('layouts.app')

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">List permissions to {{$role->name}}</div>
                <div class="panel-body">
                   @include('admin.includes.breadcrumbs')

                   <form action="{{route('role.permission.store',$role->id)}}" method="post">
           			    {{ csrf_field() }}
               			<div class="form-group col-md-5">
               				<select name="permission" class="form-control">
                                   <option value="0">Selec an permission</option>
               					@foreach($permissions as $permission)
               					<option value="{{$permission->id}}">{{$permission->name}}</option>
               					@endforeach
               				</select>
               			</div>
           				<button class="btn btn-info col-md-2" title="Add new permission"><i class="glyphicon glyphicon-plus"></i></button>
           		   </form>

                   <div class="col-md-12">
                       <table class="table table-bordered">
                           <thead>
                               <tr>
                                   <th>Permission</th>
           						   <th>Description</th>
                                   <th class="text-right">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                           @forelse($role->permissions as $permission)
                               <tr>
                                   <td>{{ $permission->name }}</td>
           			               <td>{{ $permission->description }}</td>
                                   <td class="text-right">
                                       <form action="{{route('role.permission.destroy',[$role->id,$permission->id])}}" method="post">
                          					{{ method_field('DELETE') }}
                          					{{ csrf_field() }}
                          					<button title="Delete" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                       					</form>
                                   </td>
                               </tr>
                           @empty
                               <p>No data registered</p>
                           @endforelse
                           </tbody>
                       </table>
                   </div>
             </div>
		</div>
	</div>

@endsection
