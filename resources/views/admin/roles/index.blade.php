@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Roles List</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

            <div class="col-md-12">

                <a href="{{ route('roles.create') }}" class="btn btn-info pull-right" title="Add new role" style="margin-bottom: 5px;"><i class="glyphicon glyphicon-plus"></i></a>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created At</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($roles as $role)
                        <tr>
                            <th>{{ $role->id }}</th>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->description }}</td>
                            <td>{{ $role->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('role.permission', $role->id) }}" class="btn btn-primary" title="Permissions"><i class="glyphicon glyphicon-cog"></i></a>
                                @if ($role->name == "Admin")
                                @else
                                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-default" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <form action="{{route('roles.destroy',$role->id)}}" method="post">
                                        {{ method_field('DELETE') }}
        								{{ csrf_field() }}
                                        <button class="btn btn-danger" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <p>No data registered</p>
                    @endforelse
                    </tbody>
                </table>
            </div>

        </div>
        </div>
    </div>
</div>
@endsection
