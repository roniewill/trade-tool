@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
            <div class="panel-heading">Roles list to {{$user->name}}</div>
            <div class="panel-body">
                @include('admin.includes.breadcrumbs')

                <form action="{{route('user.role.store',$user->id)}}" method="post">
    			    {{ csrf_field() }}
        			<div class="form-group col-md-5">
        				<select name="role" class="form-control">
                            <option value="0">Selec a role</option>
        					@foreach($roles as $role)
        					<option value="{{$role->id}}">{{$role->name}}</option>
        					@endforeach
        				</select>
        			</div>
    				<button class="btn btn-info col-md-2" title="Add new role"><i class="glyphicon glyphicon-plus"></i></button>
    			</form>

                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Role</th>
        						<th>Description</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($user->roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
        						<td>{{ $role->description }}</td>
                                <td class="text-right">
                                    <form action="{{route('user.role.destroy',[$user->id,$role->id])}}" method="post">
    									{{ method_field('DELETE') }}
    									{{ csrf_field() }}
    									<button title="Delete" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
        							</form>
                                </td>
                            </tr>
                        @empty
                            <p>No data registered</p>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

		</div>

	</div>

@endsection
