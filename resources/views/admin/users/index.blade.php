@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Users List</div>
            <div class="panel-body">
                @include('admin.includes.breadcrumbs')

        		<a href="{{ route('users.create') }}" 
        			class="btn btn-primary" 
        			title="Add New User">
        			
        			<i class="glyphicon glyphicon-user"></i>
        		</a>

                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Created At</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $user)
                            <tr>
                                <th>{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td class="text-right">
                                    <form action="{{route('users.destroy',$user->id)}}" method="post">
                                    <a href="{{ route('user.role',$user->id) }}" class="btn btn-primary" title="Roles"><i class="glyphicon glyphicon-cog"></i></a>
                                    <a href="{{ route('users.show', $user->id) }}" class="btn btn-info" title="Show"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button class="btn btn-danger" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <p>No data registered</p>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
