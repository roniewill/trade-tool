<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" value="{{ isset($user->name) ? $user->name : '' }}">
    @if ($errors->has('name'))
	    <span class="help-block">
	        <strong>{{ $errors->first('name') }}</strong>
	    </span>
	@endif
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    <input type="text" class="form-control" name="email" value="{{ isset($user->email) ? $user->email : '' }}">
    @if ($errors->has('email'))
	    <span class="help-block">
	        <strong>{{ $errors->first('email') }}</strong>
	    </span>
	@endif
</div>

<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" value="">
    @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="confir-password">Confirm Password</label>
    <input type="password" class="form-control" name="confirm-password" value="">
</div>
