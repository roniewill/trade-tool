@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">{{ (Auth::user()->id == $user->id) ? 'Your Profile' : 'Show User' }}</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

            <div class="col-md-8 col-md-offset-2">
                <ul class="list-group">
                    <li class="list-group-item">ID: #{{ $user->id }}</li>
                    <li class="list-group-item">Name: {{ $user->name }}</li>
                    <li class="list-group-item">E-mail: {{ $user->email }}</li>
                    <li class="list-group-item">Updated at: {{ $user->updated_at }}</li>
                </ul>

                <a href="{{ route('users.index') }}" class="btn btn-info">Go Back</a>
                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default">Edit</a>
            </div>

        </div>
    </div>
    <hr>
    <div class="panel panel-default">
        <div class="panel-heading">My API</div>
        <div class="panel-body">
            @if (isset($api))
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Detail encrypted <a href="{{ route('trade.index') }}" class="btn btn-info pull-right"> Go Trade</a></th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>{{ substr($api->secret, -5).'########################################' }}</th>
                        <td class="text-right">
                            <a href="{{ route('api.edit', $api->user_id) }}" class="btn btn-default" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a href="{{ route('api.destroy', $api->user_id) }}" class="btn btn-danger" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            @else
            <p>No API registered!  <a href="{{ route('api.create') }}" class="btn btn-primary">Add New</a></p>
            @endif
        </div>
    </div>
    
</div>
@endsection
