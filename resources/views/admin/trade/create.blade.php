@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Regiter your API</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

        	<div class="col-md-12">
        		<form action="{{ route('api.store') }}" method="post">

        			{{csrf_field()}}
        			@include('admin.trade._form_api')

        	        <button class="btn btn-success">Save</button>

        		</form>

        	</div>
        </div>
    </div>

</div>

@endsection
