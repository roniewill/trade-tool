@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Buy and Sell Coins</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

            <div class="col-md-8">
              <h3>BUY AND SELL MANUALLY</h3>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="pairs">PAIRS</label>
                <select class="form-control" name="pairs">
                  <option value="default">Select an pair</option>
                  @forelse ($pairs as $pair)
                    <option value="{{ $pair }}">{{ $pair }}</option>
                  @empty
                    <option value="no">Sorry! No options</option>
                  @endforelse
                </select>
              </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <h3 class="text-center">BUY <span class="label label-default">?</span></h3>
                    <hr>
                    <form class="form" name="buy-form">
                      {{csrf_field()}}
                      <div class="form-group">
                          <label for="buy-rate">Price BTC:</label>
                          <input type="text" class="form-control" id="buy-rate" name="buy-rate" placeholder="Price exe: 0.10270000">
                      </div>
                      <div class="form-group">
                          <label for="amount">Amount:</label>
                          <input type="text" class="form-control" id="buy-amount" name="buy-amount" placeholder="Your amount">
                      </div>
                      <div class="form-group">
                          <label for="buy-total">Total:</label>
                          <input type="text" class="form-control" id="buy-total" name="buy-total" placeholder="Total" disabled>
                      </div>
                      <div class="form-group">
                        <input type="hidden" name="buy-pair" value="">
                        <button type="submit" id="btn-buy" class="btn btn-primary">BUY</button>
                      </div>
                    </form>
                </div>
            </div>

            <div class="col-md-6">
                <div class="well">
                    <h3 class="text-center">SELL <span class="label label-default">?</span></h3>
                    <hr>
                    <form class="form" name="sell-form">
                      {{csrf_field()}}
                      <div class="form-group">
                          <label for="sell-rate">Price BTC:</label>
                          <input type="text" class="form-control" id="sell-rate" name="sell-rate" placeholder="Price exe: 0.10270000">
                      </div>
                      <div class="form-group">
                          <label for="amount">Amount:</label>
                          <input type="text" class="form-control" id="sell-amount" name="sell-amount" placeholder="Your amount">
                      </div>
                      <div class="form-group">
                          <label for="total">Total:</label>
                          <input type="text" class="form-control" id="sell-total" name="sell-total" placeholder="Total" disabled>
                      </div>
                      <div class="form-group">
                        <input type="hidden" name="sell-pair" value="">
                        <button type="submit" id="btn-sell" class="btn btn-primary">SELL</button>
                      </div>
                    </form>
                </div>
            </div>

            <div class="col-md-12">
                <h3>MY OPEN ORDERS</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Price(BTC)</th>
                            <th>Amount(setvar)</th>
                            <th>Total(setvar)</th>
                            <th>Rate/Stop</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>#</td>
                            <td>#</td>
                            <td>#</td>
                            <td>#</td>
                            <td>#</td>
                            <td>#</td>
                            <td>#</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection