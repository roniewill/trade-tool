<div class="form-group">
    <label for="key">KEY</label>
    <input type="text" class="form-control" name="key" value="{{ isset($api->key) ? $api->key : '' }}">
</div>

<div class="form-group">
    <label for="description">SECRET</label>
    <input type="text" class="form-control" name="secret" value="{{ isset($api->secret) ? $api->secret : '' }}">
</div>

<input type="hidden" name="user_id" value="{{ isset($api->user_id) ? $api->user_id : Auth::user()->id }}">