@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Edit your API</div>
        <div class="panel-body">
            @include('admin.includes.breadcrumbs')

        	<div class="col-md-12">
        		<form action="{{ route('api.update', $api->id) }}" method="post">

        			{{csrf_field()}}
                    {{ method_field('PUT') }}
        			@include('admin.trade._form_api')

        	        <button class="btn btn-success">Update</button>

        		</form>

        	</div>
        </div>
    </div>

</div>

@endsection
